
public abstract class GameObject
{
	protected GameObject [][] board;
		
	public String toString()
	{
		StringBuilder sb = new StringBuilder();		
		for (int y = 0; y < board.length; y++) {
			for (int x = 0; x < board[0].length; x++) {
				sb.append(board[x][y]);
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
